package test.jenkins.fenetre;

import java.awt.BorderLayout;
import java.awt.GridLayout ;
import java.awt.event.ActionEvent ;
import java.awt.event.ActionListener ;

import javax.swing.JButton ;
import javax.swing.JFrame ;
import javax.swing.JLabel ;
import javax.swing.JPanel ;
import javax.swing.JTextField ;

import test.jenkins.modele.JenkinsOne ;
import test.jenkins.service.JenkinsOneService;

public class JenkinsOneView implements ActionListener
{
	private JFrame frame;
	private JPanel panel;
	private JLabel zone1Label;
	private JLabel zone2Label;
	private JLabel zone3Label;
	private JLabel zone4Label;
	private JTextField zone1Champ;
	private JTextField zone2Champ;
	private JTextField zone3Champ;
	private JTextField zone4Champ;
	private JButton buttonJenkis;
	private JenkinsOneService jenkisService;
	private JPanel panelGlobal;
	private JLabel titleLabel;
	
	public JenkinsOneView(){
		frame = new JFrame ("Jenkis Test View");
		frame.setSize (300, 200);
		jenkisService = new JenkinsOneService ();
		panelGlobal = new JPanel(new BorderLayout());
		titleLabel = new JLabel("JENKIS TEST");
		panel = new JPanel (new GridLayout (0, 2));
		zone1Label = new JLabel ("zone1: ");
		zone1Champ = new JTextField (10);
		zone1Champ.setEditable (Boolean.FALSE);
		zone2Label = new JLabel ("zone2: ");
		zone2Champ = new JTextField(10);
		zone2Champ.setEditable (Boolean.FALSE);
		zone3Label = new JLabel ("zone3: ");
		zone3Champ = new JTextField (10);
		zone3Champ.setEditable (Boolean.FALSE);
		zone4Label = new JLabel("zone4: ");
		zone4Champ = new JTextField (10);
		zone4Champ.setEditable (Boolean.FALSE);

		buttonJenkis = new JButton ("Load data");
		buttonJenkis.addActionListener (this);
		panel.add (zone1Label);
		panel.add(zone1Champ);
		panel.add (zone2Label);
		panel.add(zone2Champ);
		panel.add (zone3Label);
		panel.add(zone3Champ);
		panel.add (zone4Label);
		panel.add(zone4Champ);
		panel.add (buttonJenkis);
		panelGlobal.add(BorderLayout.PAGE_START,titleLabel);
		panelGlobal.add(BorderLayout.CENTER,panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(Boolean.FALSE);
		frame.add (panelGlobal);
		frame.setLocationRelativeTo(null);
		frame.setVisible (Boolean.TRUE);
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource () == buttonJenkis){
			JenkinsOne jenkisOne = jenkisService.getJenkisOne ();
			zone1Champ.setText (jenkisOne.getString1 ());
			zone2Champ.setText (jenkisOne.getString2 ());
			jenkisOne = jenkisService.getJenkisOneNonTeste ();
			zone3Champ.setText (jenkisOne.getString1 ());
			zone4Champ.setText (jenkisOne.getString2 ());
		}
		
	}

}
