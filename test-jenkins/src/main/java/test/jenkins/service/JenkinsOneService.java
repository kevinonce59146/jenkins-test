package test.jenkins.service;

import test.jenkins.modele.JenkinsOne ;

public class JenkinsOneService
{
	private static String uselessString = "useless";
	public JenkinsOne getJenkisOne(){
		JenkinsOne jenkisOne = new JenkinsOne ("arg1", "arg2");
		return jenkisOne;
	}
	
	public JenkinsOne getJenkisOneNonTeste(){
		JenkinsOne jenkisOne = new JenkinsOne ("test1", "test2");
		return jenkisOne;
	}
}
