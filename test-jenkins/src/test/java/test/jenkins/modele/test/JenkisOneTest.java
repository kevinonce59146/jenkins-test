package test.jenkins.modele.test;

import test.jenkins.modele.JenkinsOne ;
import junit.framework.TestCase ;

public class JenkisOneTest extends TestCase
{
	private JenkinsOne jenkisOneGet;
	private JenkinsOne jenkisOneSet;
	
	public JenkisOneTest(String name)
	{
		super (name) ;
		jenkisOneGet = new JenkinsOne ("test1", "test2");
		jenkisOneSet = new JenkinsOne ();
	}

	public void testGetString1()
	{
		assertEquals ("test1", jenkisOneGet.getString1 ());
	}

	public void testSetString1()
	{
		jenkisOneSet.setString1 ("settest");
		assertEquals("settest",jenkisOneSet.getString1 ());
	}

	public void testGetString2()
	{
		assertEquals ("test2", jenkisOneGet.getString2 ());
	}

	public void testSetString2()
	{
		jenkisOneSet.setString2 ("settest2");
		assertEquals("settest2",jenkisOneSet.getString2 ());
	}

}
