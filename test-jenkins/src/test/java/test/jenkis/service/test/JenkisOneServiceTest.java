package test.jenkis.service.test;

import test.jenkins.modele.JenkinsOne ;
import test.jenkins.service.JenkinsOneService;
import junit.framework.TestCase ;

public class JenkisOneServiceTest extends TestCase
{
	private JenkinsOneService jenkisOneService;
	public JenkisOneServiceTest(String name)
	{
		super (name) ;
		
		jenkisOneService = new JenkinsOneService ();
	}

	public void testGetJenkisOne()
	{
		JenkinsOne jenkisOne = jenkisOneService.getJenkisOne ();
		assertEquals ("arg1", jenkisOne.getString1 ());
		assertEquals("arg2",jenkisOne.getString2 ());
	}

}
